# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils

MY_P="Markdown_${PV}"
DEBIAN_PATCH="markdown_1.0.1-7.diff.gz"

DESCRIPTION="A text-to-HTML conversion tool for web writers."
HOMEPAGE="http://daringfireball.net/projects/markdown/"
SRC_URI="http://daringfireball.net/projects/downloads/${MY_P}.zip
	mirror://debian/pool/main/m/${PN}/${DEBIAN_PATCH}"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

DEPEND="app-arch/unzip"
RDEPEND=">=dev-lang/perl-5.6.0
	virtual/perl-Digest-MD5"

S="${WORKDIR}/${MY_P}"

src_unpack() {
	unpack ${MY_P}.zip
	cd "${S}"
	epatch "${DISTDIR}/${DEBIAN_PATCH}"
}

src_install() {
	newbin Markdown.pl markdown || die "newbin failed"

	# Make it available as a perl module too
	local installvendorlib
	eval `perl '-V:installvendorlib'`
	VENDOR_LIB=${installvendorlib}
	dodir "${VENDOR_LIB}"/Text
	dosym /usr/bin/markdown "${VENDOR_LIB}"/Text/Markdown.pm

	newdoc 'Markdown Readme.text' README
}
