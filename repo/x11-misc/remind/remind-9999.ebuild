# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/x11-misc/remind/remind-03.01.03.ebuild,v 1.1 2007/10/17 15:25:03 tove Exp $

MY_P=${P/_beta/-BETA-}
if [[ ${PV} == 9999 ]] ; then
	scm=true
	inherit git
	EGIT_REPO_URI="http://git.roaringpenguin.com/Remind.git"
	KEYWORDS=""
else
	SRC_URI="http://www.roaringpenguin.com/files/download/${MY_P}.tar.gz"
	KEYWORDS="~amd64 ~ppc ~sparc ~x86 ~x86-fbsd"
fi

DESCRIPTION="Ridiculously functional reminder program"
HOMEPAGE="http://www.roaringpenguin.com/products/remind"
LICENSE="GPL-2"
SLOT="0"
IUSE="tk"

RDEPEND="tk? ( dev-lang/tk dev-tcltk/tcllib )"

S=${WORKDIR}/${MY_P}

src_unpack() {
	if [[ -n "${scm}" ]] ; then
		git_src_unpack
	else
		unpack ${A}
	fi
	sed -i 's:$(MAKE) install:&-nostripped:' "${S}"/Makefile || die
}

src_install() {
	emake DESTDIR="${D}" install || die "make install failed"
	dobin www/rem2html || die "dobin failed"

	dodoc docs/WHATSNEW examples/defs.rem www/README.* || die "dodoc failed"

	if ! use tk ; then
		rm "${D}"/usr/bin/tkremind "${D}"/usr/share/man/man1/tkremind* \
			"${D}"/usr/bin/cm2rem*  "${D}"/usr/share/man/man1/cm2rem*
	fi
}
