# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3

inherit perl-module toolchain-funcs

DESCRIPTION="regenerate a pristine upstream tarball"
HOMEPAGE="http://kitenet.net/~joey/code/pristine-tar/"
SRC_URI="mirror://debian/pool/main/p/${PN}/${PN}_${PV}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="sys-libs/zlib
	dev-lang/perl"
RDEPEND="dev-lang/perl
	dev-util/xdelta:0"

S=${WORKDIR}/${PN}

src_prepare(){
	sed -i "s/gcc -Wall -O2 -lz -o \$@ \$(ZGZ_SOURCES)/$(tc-getCC) \$(CFLAGS) \$(LDFLAGS) -o \$@ \$(ZGZ_SOURCES) -lz/" \
		"${S}"/Makefile.PL || die
	perl-module_src_prepare
}

src_install(){
	mydoc="delta-format.txt debian/changelog"
	perl-module_src_install
}
